package com.example.webservice;


import com.example.webservice.domain.Employee;
import com.example.webservice.domain.Project;
import com.example.webservice.repos.EmployeeRepo;
import com.example.webservice.repos.ProjectRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Controller
public class GreetingController {

    @Autowired
    private EmployeeRepo employeeRepo;

    @Autowired
    private ProjectRepo projectRepo;

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = false, defaultValue = "World") String name,
                           Map<String, Object> model
    ) {
        model.put("name", name);
        return "greeting";
    }

    @GetMapping("/employees")
    public String employees(Map<String, Object> model) {
        Iterable<Employee> employees = employeeRepo.findAll();
        model.put("employees", employees);
        return "employees";
    }

    @GetMapping("/projects")
    public String projects(Map<String, Object> model) {
        Iterable<Project> projects = projectRepo.findAll();
        model.put("projects", projects);
        return "projects";
    }

    @PostMapping
    public String addEmployee(@RequestParam String firstname, @RequestParam String lastname
            , Map<String, Object> model) {

//        project.setName(projectName);

        Employee employee = new Employee();

        employee.setFirstname(firstname);
        employee.setLastname(lastname);

        employeeRepo.save(employee);

        Iterable<Employee> employees = employeeRepo.findAll();

        model.put("employees", employees);

        Set<Employee> newset = new HashSet<>();

        employees.forEach(newset::add);

//        Project pr = new Project( );
//        pr.setName(projectName);
//        pr.setEmployees(newset);
//        projectRepo.save(pr);
//        Iterable<Project> projects = projectRepo.findAll();

        return "employees";
    }

}