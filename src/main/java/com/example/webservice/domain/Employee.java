package com.example.webservice.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "employee")
public class Employee implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "employee_publicid")
    private Long publicid;

    private String firstname;

    private String lastname;

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "project_publicid")
    private Project project;


    public Employee() {
    }


    public Employee(String firstname, String lastname, Project project) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.project = project;
    }

    public Long getPublicid() {
        return publicid;
    }

    public void setPublicid(Long publicid) {
        this.publicid = publicid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "publicid=" + publicid +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", project=" + project +
                '}';
    }

/*    <div>
        <b> {{publicid}} </b>
        <span> {{firstname}} </span>
        <span> {{lastname}} </span>
        <i> {{project}} </i>
    </div>*/
}
