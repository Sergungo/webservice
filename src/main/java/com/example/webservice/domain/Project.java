package com.example.webservice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;


@Entity
@Table(name = "project")
public class Project implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "project_publicid")
    private Long publicid;
    private String name;
    private String description;
    private String client;

    @OneToMany(mappedBy = "project", fetch = FetchType.EAGER)
    private Set<Employee> employees;

    public Project() {
    }

    public Project(String name, String description, String client, Set<Employee> employees) {
        this.name = name;
        this.description = description;
        this.client = client;
        this.employees = employees;
    }

    public Long getPublicid() {
        return publicid;
    }

    public void setPublicid(Long publicid) {
        this.publicid = publicid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public String toString() {
        return name;
    }


}