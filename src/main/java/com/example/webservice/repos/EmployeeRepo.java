package com.example.webservice.repos;

import com.example.webservice.domain.Employee;
import com.example.webservice.domain.Project;
import org.springframework.data.repository.CrudRepository;

import java.util.HashSet;

public interface EmployeeRepo extends CrudRepository<Employee, Long> {

    HashSet<Employee> findByProject(Project project);


}
