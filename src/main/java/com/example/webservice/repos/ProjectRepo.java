package com.example.webservice.repos;

import com.example.webservice.domain.Project;
import org.springframework.data.repository.CrudRepository;

public interface ProjectRepo extends CrudRepository<Project, Long> {

//    List<Project> findByID(long publicid);
}
